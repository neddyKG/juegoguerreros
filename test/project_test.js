let expect = require('chai').expect;
let should = require('chai').should();
let CalcularDanioGuerrero = require('../CalcularDanioGuerreroStrategy/CalcularDanioGuerrero');

describe('Guerrero test', function() {

    it('deberia iniciar un guerrero de infanteria con 5 de daño', function() {   
        let calcularDanioGuerrero = new CalcularDanioGuerrero('infanteria');   
        expect(calcularDanioGuerrero.calcularDanioGuerrero()).equal(5);
    });

    it('deberia iniciar un guerrero arquero con 10 de daño', function() {   
        let calcularDanioGuerrero = new CalcularDanioGuerrero('arquero');   
        expect(calcularDanioGuerrero.calcularDanioGuerrero()).equal(10);
    });

    it('deberia crear arma de fuego para un guerrero', function() {   
        //TO-DO
        let calcularDanioGuerrero = new CalcularDanioGuerrero('arquero');   
         expect(calcularDanioGuerrero.calcularDanioGuerrero()).equal(10);
    });

});


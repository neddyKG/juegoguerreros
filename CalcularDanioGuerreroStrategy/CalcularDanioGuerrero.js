let DanioInfanteria = require('./DanioInfanteria');
let DanioArquero = require('./DanioArquero');

class CalcularDanioGuerrero{
    constructor(tipo){
        this.tipo = tipo;
        this.danioGuerreroTipo = this.creardanioGuerreroTipo();
    }

    calcularDanioGuerrero(){
        return this.danioGuerreroTipo.calcularDanioGuerrero();
    }

    creardanioGuerreroTipo(){
        switch(this.tipo){
            case 'infanteria':
                return new DanioInfanteria();
            case 'arquero':
                return new DanioArquero();
        }
    }
}

module.exports = CalcularDanioGuerrero;

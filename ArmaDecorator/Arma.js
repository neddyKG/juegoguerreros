let DanioInfanteria = require('./DanioInfanteria');
let DanioArquero = require('./DanioArquero');

class Arma{
    constructor(tipo){
        this.tipo = tipo;
        this.tipoArma = this.crearArma();
    }

    calcularDanio(){
        return this.tipoArma.calcularDanio();
    }

    crearArma(){
        switch(this.tipo){
            case 'arco':
                return new Arco();
            case 'pistola':
                return new Pistola();
        }
    }
}

module.exports = Arma;
